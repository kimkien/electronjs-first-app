const {app, BrowserWindow} = require('electron')
const path = require('path')

function createWindow () {
  const express = require('express')
  const axios = require('axios')
  const web = express()
  var parseString = require('xml2js').parseString;
  web.get('/', (req, res) => {
    
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    axios.get('https://vnexpress.net/rss/thoi-su.rss').then((response) => {
      parseString(response.data, function (err, result) {
        var posts = result.rss.channel[0].item;
        var data = [];
        posts.forEach((post) => {
          data.push({
            'title' : post.title[0],
            'link' : post.link[0],
            'description' : post.description[0].toString().replace('<img', '<img style="display: none" ')
          })
        });
        res.json(data);
      });
    }).catch(error => {
        console.log(error)
    }); 
  })  
  web.listen(8181)


  const mainWindow = new BrowserWindow()
  mainWindow.loadFile('./html/dist/index.html')
  //mainWindow.loadURL('http://localhost:8080')
  mainWindow.setAutoHideMenuBar(true)
  
}
app.whenReady().then(createWindow)
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  if (BrowserWindow.getAllWindows().length === 0) createWindow()
})